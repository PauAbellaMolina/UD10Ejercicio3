import java.util.Random;
import exceptions.CustomException;

public class CustomExceptionApp {

	public static void main(String[] args) {
		Random random = new Random();
		
		//Generamos un numero aleatorio
		int numeroRandom = random.nextInt(1000);
		
		System.out.println("Generando numero aleatorio...");
		System.out.println("El numero aleatorio generado es: " + numeroRandom);
		
		//Lanzamos la excepcion custom con un mensaje o otro dependiendo de si el numero generado aleatoriamente es par o impar
		//Luego recojemos la excepcion y mostramos el mensaje pasado anteriormente para ver por consola si el numero es par o impar
		try {
			if (numeroRandom%2 == 0) {
				throw new CustomException("par");
			} else {
				throw new CustomException("impar");
			}
		}catch(CustomException e) {
			System.out.println("Es " + e);
		}
	}
}
