package exceptions;

public class CustomException extends Exception {
	//Atributo
	protected String message;
	
	//Constructor por defecto
	public CustomException() {
		this.message="";
	}
	
	//Constructor que recibe un string y lo asigna al atributo mensaje
	public CustomException(String message) {
		this.message=message;
	}

	//Metodo toString que devuelve el atributo mensaje
	public String toString() {
		return message;
	}
}
